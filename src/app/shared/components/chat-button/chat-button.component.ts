import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-chat-button',
  templateUrl: './chat-button.component.html',
  styleUrls: ['./chat-button.component.scss']
})
export class ChatButtonComponent implements OnInit {
  public showHide:boolean = false;
  
  constructor() { }

  ngOnInit(): void {
  }

}
