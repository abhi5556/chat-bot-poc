import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-chat-model',
  templateUrl: './chat-model.component.html',
  styleUrls: ['./chat-model.component.scss']
})
export class ChatModelComponent implements OnInit {
  
  public faqLink: string = "https://support.pl--tst.thehartford.com/app/answers/answer_view_cs/a_id/1000191";
  constructor() { }

  ngOnInit(): void {
  }

}
