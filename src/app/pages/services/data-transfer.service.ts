import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataTransferService {
  private dataSource = new BehaviorSubject<string>('');
  data = this.dataSource.asObservable();

  constructor() { }

  updatedDataSelection(data: string){
    this.dataSource.next(data);
  }

  
}
