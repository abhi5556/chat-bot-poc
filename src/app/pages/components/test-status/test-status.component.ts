import { Component, OnInit, Output } from '@angular/core';
import { EventEmitter } from 'events';
import { Subject } from 'rxjs';
import { DataTransferService } from '../../services/data-transfer.service';

@Component({
  selector: 'app-test-status',
  templateUrl: './test-status.component.html',
  styleUrls: ['./test-status.component.scss']
})
export class TestStatusComponent implements OnInit {
  selected = 'info';
  
  constructor(public dataTransferService:DataTransferService) { }

  ngOnInit(): void {
    this.dataTransferService.updatedDataSelection(this.selected);
  }

  panelChangeClick():void {
    this.dataTransferService.updatedDataSelection(this.selected);
  }
}
