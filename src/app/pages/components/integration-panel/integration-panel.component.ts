import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { DataTransferService } from '../../services/data-transfer.service';

@Component({
  selector: 'app-integration-panel',
  templateUrl: './integration-panel.component.html',
  styleUrls: ['./integration-panel.component.scss']
})
export class IntegrationPanelComponent implements OnInit,OnChanges {
  public status:string;
  constructor(private dataTransferService:DataTransferService) { }

  ngOnInit(): void {
    let stts = this.dataTransferService.data.subscribe(data => {this.status = data;})
  }

  ngOnChanges(){
    let stts = this.dataTransferService.data.subscribe(data => {this.status = data;})
  }
}
