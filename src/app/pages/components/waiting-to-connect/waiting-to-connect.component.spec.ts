import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WaitingToConnectComponent } from './waiting-to-connect.component';

describe('WaitingToConnectComponent', () => {
  let component: WaitingToConnectComponent;
  let fixture: ComponentFixture<WaitingToConnectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WaitingToConnectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WaitingToConnectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
