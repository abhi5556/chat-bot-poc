import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-waiting-to-connect',
  templateUrl: './waiting-to-connect.component.html',
  styleUrls: ['./waiting-to-connect.component.scss']
})
export class WaitingToConnectComponent implements OnInit {
  public queueNumber:number = 1;
  constructor() { }

  ngOnInit(): void {
  }

}
