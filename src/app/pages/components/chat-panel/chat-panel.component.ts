import { Component, OnInit } from '@angular/core';
import { ChatTranscript } from '../../model/chat-transcript';

@Component({
  selector: 'app-chat-panel',
  templateUrl: './chat-panel.component.html',
  styleUrls: ['./chat-panel.component.scss']
})
export class ChatPanelComponent implements OnInit {

  public chatTranscript:ChatTranscript[] = [
    {userAgent:true,text:"Hi"},
    {userAgent:false,text:`i can conclude that you declared LoginComponent in AppRoutingModule but
    didn't import MatFormFieldModule there.`},
    {userAgent:true,text:"Hi"},
    {userAgent:false,text:`i can conclude that you declared LoginComponent in AppRoutingModule but
    didn't import MatFormFieldModule there.`},
    {userAgent:true,text:"Hi"},
    {userAgent:false,text:`i can conclude that you declared LoginComponent in AppRoutingModule but
    didn't import MatFormFieldModule there.`},
    {userAgent:true,text:"Hi"},
    {userAgent:false,text:`i can conclude that you declared LoginComponent in AppRoutingModule but
    didn't import MatFormFieldModule there.`},
    {userAgent:true,text:"Hi"},
    {userAgent:false,text:`i can conclude that you declared LoginComponent in AppRoutingModule but
    didn't import MatFormFieldModule there.`},
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
