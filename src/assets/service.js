if(!window._genesys.widgets.extensions){

    window._genesys.widgets.extensions = {};
}

window._genesys.widgets.extensions["TestExtension"] = function($, CXBus, Common){

    var oTestExtension = CXBus.registerPlugin("TestExtension");

    oTestExtension.subscribe("WebChat.opened", function(e){});

    oTestExtension.publish("ready");

    oTestExtension.command("WebChat.open").done(function(e){

          // Handle success return state

    }).fail(function(e){

          // Handle failure return state
    });

    oTestExtension.registerCommand("demo", function(e){

          // Command execution here
    });
};