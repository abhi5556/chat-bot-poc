//from sean
// window._genesys.widgets = {
//       main: {
//             debug: true,
//             theme: "dark",
//             lang: "en",
//             customStylesheetID: "genesys_widgets_custom",
//             preload: []
//       },
//       webchat: {
//             userData: {},
//             emojis: true,
//             cometD: {
//                   enabled: false
//             },
//             autoInvite: {
//                   enabled: false,
//                   timeToInviteSeconds: 5,
//                   inviteTimeoutSeconds: 30
//             },
//             chatButton: {
//                   enabled: true,
//                   openDelay: 1000,
//                   effectDuration: 300,
//                   hideDuringInvite: true
//             },
//             uploadsEnabled: true,
//             enableCustomHeader: true,
//             transport: {
//                   dataURL: "https://digital-use4.genesyscloud.com/nexus/v3/chat/sessions",
//                   type: "pureengage-v3-rest",
//                   endpoint: "CE18",
//                   headers: {
//                         'x-api-key': "91979375-e6b4-497e-b348-a0c1d242876e"
//                   },
//                   stream: "dev"
//             }
//       }
// };

//from genesys site

if (!window._genesys) window._genesys = {};
if (!window._gt) window._gt = [];

window._genesys.widgets = {
  main: {
    debig: true,
    theme: "dark",
    lang: "en",
    customStylesheetID: "genesys_widgets_custom",
    preload: []
  },
  webchat: {
    userData: {},
    emojis: true,
    cometD: {
      enabled: false
    },
    autoInvite: {
      enabled: false,
      timeToInviteSeconds: 5,
      inviteTimeoutSeconds: 30
    },
    chatButton: {
      enabled: false,
      openDelay: 1000,
      effectDuration: 300,
      hideDuringInvite: true
    },
    uploadsEnabled: true,
    enableCustomHeader: true,
    transport: {
      dataURL: "https://digital-use4.genesyscloud.com/nexus/v3/chat/sessions",
      type: "pureengage-v3-rest",
      endpoint: "CE18",
      headers: {
        "x-api-key": "91979375-e6b4-497e-b348-a0c1d242876e"
      },
      stream: "dev",
      async: {
        enabled: true,

        getSessionData: function(sessionData, Cookie, CookieOptions) {
          // Note: You don't have to use Cookies. You can, instead, store in a secured location like a database.
          Cookie.set(
            "customer-defined-session-cookie",
            JSON.stringify(sessionData),
            CookieOptions
          );
        },

        setSessionData: function(Open, Cookie, CookieOptions) {
          // Retrieve from your secured location.
          return Cookie.get("customer-defined-session-cookie");
        }
      }
    }
  }
};

//curl POST -H "x-api-key:91979375-e6b4-497e-b348-a0c1d242876e" -H "Content-Type: application/json" -d '{}' "https://digital-use4.genesyscloud.com/nexus/v3/chat/sessions?"

// (function(o) {
//   var f = function() {
//       var d = o.location;
//       o.aTags = o.aTags || [];
//       for (var i = 0; i < o.aTags.length; i++) {
//         var oTag = o.aTags[i];
//         var fs = d.getElementsByTagName(oTag.type)[0],
//           e;
//         if (d.getElementById(oTag.id)) return;
//         e = d.createElement(oTag.type);
//         e.id = oTag.id;
//         if (oTag.type == "script") {
//           e.src = oTag.path;
//         } else {
//           e.type = "text/css";
//           e.rel = "stylesheet";
//           e.href = oTag.path;
//         }
//         if (fs) {
//           fs.parentNode.insertBefore(e, fs);
//         } else {
//           d.head.appendChild(e);
//         }
//       }
//     },
//     ol = window.onload;
//   if (o.onload) {
//     typeof window.onload != "function"
//       ? (window.onload = f)
//       : (window.onload = function() {
//           ol();
//           f();
//         });
//   } else f();
// })({
//   location: document,
//   onload: false,
//   aTags: [
//     {
//       type: "script",
//       id: "genesys-cx-widget-script",
//       path:
//         "https://thehartford-consumer--tst1.custhelp.com/euf/assets/themes/js/genesys/widgets.min.js"
//     },
//     {
//       type: "link",
//       id: "genesys-cx-widget-styles",
//       path:
//         "https://thehartford-consumer--tst1.custhelp.com/euf/assets/themes/js/genesys/widgets.min.css"
//     }
//   ]
// });
